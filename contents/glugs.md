GLUG Checklist
===============
* Fix the sessions for the next week meetup in the previous meeting itself. This would avoid confusions and leads to a clear decision in terms of GLUG interests
* Finalise the Poster on Wednesday itself for the meetup on Sunday. Reach out to maximum college students in the next three days through our Social media handles
* Publish our event in [Meetup](https://meetup.com) and share the link in social media. Provide the complete information
* Fix the sessions for the meetup - One with specialised content with some external speaker and other with one our GLUG member who is learning something new
* Invite new volunteers to take sessions and train them accordingly and give productive feedback
* Core GLUG team shoudl discuss the progress of the meetup in individual and collective manner. This is will be the place to review the GLUG progress by the organising team
* Motivate and Encourage the interests of the volunteers of the GLUG which directly contributes to the growth of the GLUG
* Share the Meeting info such as Summary, Event Details, Activity, etc in Mailing List and our [Discussion Forum](https://discuss.fsftn.org)
* Invite voluteers from new colleges in your region for every meetup and follow up with them. This will expand our contacts with most of the colleges around the region
* Get feedback from the attendees after the meetup, about the sessions and other activites
* Plan for a Civic Action once in a month and engage active volunteers in organising it
* Record the session of every meetup using tools like OBS or live stream it directly in our organisation handle
* Upload the recorded videos in Peertube and Youtube and share the links
* Share the pics taken in our Social Media and upload it in Flickr after the meetup
* Spend time with the volunteers after the meetup as it would increase their bonding with us
* Gather contacts of new volunteers in every meeting
