Sessions Checklist
===================
* Gather details about the Venue either it is College, Institution, School, etc.
* Make note of the target audience and no. of people you are going to address
* Ask interested volunteers to take the session. One Core member is recommended
* Take **FSFTN Card** along with you which has all the details in it.
* Select session topics with a combination of Talks, Technical Sessions and Hands-on. Make sure you touch the **Free Software** and **Knowledge Commons** aspect in each of these.
* Interaction with the participants in most important
* Make a note of the session organizers (from School or College) and team leaders. It is a +1 for leadership skills.
* Get the details of the Contact person from the Venue 3 or 4 days prior to the session
* Communicate the speaker profile of the volunteers to the contact person
* Make a personal checklist of things to do on the day before
* After the session, have a interactive talk with the staffs and coordinators about the usage of FOSS in their college, forming a User group in their college. This would nurture the relationship with the college in a better way
* Don't forget to take a group picture at the end of the session. This would serve as a memory and future inspiration :-)
* After the technical sessions have been completed, at the end, Take about FSFTN and why we are here and how they could join us.
* Talk about GLUG formation and the knowledge sharing perspective of it.
* Before leaving, ask written feedback from the participants. This is would help us review our standards.
* After reaching home, make sure we have a review meeting about what happened there, with other volunteers either through Phone or Direct meetings.
* Post pictures of the session in Facebook, Twitter, Diaspora
* Make a writeup on it on our [Discussion Forum](https://discuss.fsftn.org)
* With the writeup send a mail to mailinglist@fsftn.org with CC to core@fsftn.org
* Submit expenses details for the session during the review meeting
